# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the letterpress package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: letterpress\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-19 23:52+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Translate as "Letterpress"
#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:4 src/gtk/window.blp:5
msgid "Letterpress"
msgstr ""

#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:5
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:11
msgid "Create beautiful ASCII art"
msgstr ""

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:14
msgid "image;ascii;convert;conversion;text;"
msgstr ""

#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:14
msgid ""
"Letterpress converts your images into a picture made up of ASCII characters. "
"You can save the output to a file, copy it, and even change its resolution! "
"High-res output can still be viewed comfortably by lowering the zoom factor."
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:36
msgid "Copy output"
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:41
msgid "Save output"
msgstr ""

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:46
msgid "Adjust output width"
msgstr ""

#: src/main.py:124
msgid "Output copied to clipboard"
msgstr ""

#: src/main.py:169
msgid "Copyright © 2023 Letterpress Contributors"
msgstr ""

#. Translators: Translate this string as your translator credits.
#. Name only:    Gregor Niehl
#. Name + URL:   Gregor Niehl https://gitlab.gnome.org/gregorni/
#. Name + Email: Gregor Niehl <gregorniehl@web.de>
#. Do not remove existing names.
#. Names are separated with newlines.
#: src/main.py:176
msgid "translator-credits"
msgstr ""

#: src/main.py:179
msgid "Code and Design borrowed from"
msgstr ""

#. Translators: Do not translate "{basename}"
#: src/window.py:103
#, python-brace-format
msgid "“{basename}” is not of a supported image type."
msgstr ""

#: src/window.py:244
msgid "Dropped item is not a valid image"
msgstr ""

#: src/file_chooser.py:38 src/file_chooser.py:101
msgid "Select a file"
msgstr ""

#: src/file_chooser.py:45
msgid "Supported image files"
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:79
#, python-brace-format
msgid "Unable to save “{display_name}”"
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:88
#, python-brace-format
msgid "“{display_name}” saved"
msgstr ""

#: src/file_chooser.py:90
msgid "Open"
msgstr ""

#: src/pasting.py:48
msgid "No image found in clipboard"
msgstr ""

#: src/gtk/window.blp:17 src/gtk/tips-dialog.blp:5
msgid "Tips"
msgstr ""

#: src/gtk/window.blp:26
msgid "Main Menu"
msgstr ""

#: src/gtk/window.blp:90
msgid "Create ASCII Art"
msgstr ""

#: src/gtk/window.blp:91
msgid "Open or drag and drop an image to generate an ASCII art version of it"
msgstr ""

#: src/gtk/window.blp:96
msgid "Open File…"
msgstr ""

#: src/gtk/window.blp:158
msgid "Width (in characters)"
msgstr ""

#: src/gtk/window.blp:184
msgid "Copy to clipboard"
msgstr ""

#: src/gtk/window.blp:192
msgid "Save to file"
msgstr ""

#: src/gtk/window.blp:211
msgid "_Open File…"
msgstr ""

#: src/gtk/window.blp:218
msgid "_Keyboard Shortcuts"
msgstr ""

#: src/gtk/window.blp:223
msgid "_About Letterpress"
msgstr ""

#: src/gtk/zoom-box.blp:12
msgid "Zoom out"
msgstr ""

#: src/gtk/zoom-box.blp:29
msgid "Reset zoom"
msgstr ""

#: src/gtk/zoom-box.blp:42
msgid "Zoom in"
msgstr ""

#: src/gtk/tips-dialog.blp:22
msgid "Pictures used for ASCII art should have the following qualities:"
msgstr ""

#: src/gtk/tips-dialog.blp:49
msgid "High Contrast"
msgstr ""

#: src/gtk/tips-dialog.blp:66
msgid "Vibrant Colors"
msgstr ""

#: src/gtk/tips-dialog.blp:83
msgid "Defined Edges"
msgstr ""

#: src/gtk/tips-dialog.blp:100
msgid "Recognizable Shapes"
msgstr ""

#: src/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: src/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr ""

#: src/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Paste Image"
msgstr ""

#: src/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: src/gtk/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: src/gtk/help-overlay.blp:35
msgctxt "shortcut window"
msgid "Text View"
msgstr ""

#: src/gtk/help-overlay.blp:38
msgctxt "shortcut window"
msgid "Increase Output Width"
msgstr ""

#: src/gtk/help-overlay.blp:43
msgctxt "shortcut window"
msgid "Decrease Output Width"
msgstr ""

#: src/gtk/help-overlay.blp:48
msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#: src/gtk/help-overlay.blp:53
msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""

#: src/gtk/help-overlay.blp:58
msgctxt "shortcut window"
msgid "Reset zoom"
msgstr ""

#: src/gtk/help-overlay.blp:63
msgctxt "shortcut window"
msgid "Copy Output to Clipboard"
msgstr ""

#: src/gtk/help-overlay.blp:68
msgctxt "shortcut window"
msgid "Save Output to File"
msgstr ""
