# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the letterpress package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: letterpress\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-19 23:52+0300\n"
"PO-Revision-Date: 2024-05-25 14:42+0000\n"
"Last-Translator: mondstern <mondstern@monocles.de>\n"
"Language-Team: Polish <https://hosted.weblate.org/projects/letterpress/"
"letterpress/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.6-dev\n"

#. Translators: Translate as "Letterpress"
#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:4 src/gtk/window.blp:5
msgid "Letterpress"
msgstr "Letterpress"

#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:5
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:11
msgid "Create beautiful ASCII art"
msgstr "Twórz piękną grafikę ASCII"

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.Letterpress.desktop.in.in:14
msgid "image;ascii;convert;conversion;text;"
msgstr "obraz;ascii;konwersja;konwersja;tekst;"

#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:14
msgid ""
"Letterpress converts your images into a picture made up of ASCII characters. "
"You can save the output to a file, copy it, and even change its resolution! "
"High-res output can still be viewed comfortably by lowering the zoom factor."
msgstr ""
"Letterpress konwertuje obrazy na obraz składający się ze znaków ASCII. Dane "
"wyjściowe można zapisać w pliku, skopiować, a nawet zmienić ich "
"rozdzielczość! Dane wyjściowe o wysokiej rozdzielczości można nadal wygodnie "
"oglądać, zmniejszając współczynnik powiększenia."

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:36
msgid "Copy output"
msgstr "Kopiowanie wyjścia"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:41
msgid "Save output"
msgstr "Zapisz wynik"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in:46
msgid "Adjust output width"
msgstr "Regulacja szerokości wyjścia"

#: src/main.py:124
msgid "Output copied to clipboard"
msgstr "Wyjście skopiowane do schowka"

#: src/main.py:169
msgid "Copyright © 2023 Letterpress Contributors"
msgstr "Prawa autorskie © 2023 Letterpress Współtwórcy"

#. Translators: Translate this string as your translator credits.
#. Name only:    Gregor Niehl
#. Name + URL:   Gregor Niehl https://gitlab.gnome.org/gregorni/
#. Name + Email: Gregor Niehl <gregorniehl@web.de>
#. Do not remove existing names.
#. Names are separated with newlines.
#: src/main.py:176
msgid "translator-credits"
msgstr "mondstern https://moooon.dresden.network/"

#: src/main.py:179
msgid "Code and Design borrowed from"
msgstr "Kod i projekt zapożyczone z"

#. Translators: Do not translate "{basename}"
#: src/window.py:103
#, python-brace-format
msgid "“{basename}” is not of a supported image type."
msgstr "„{basename}” nie jest obsługiwanym typem obrazu."

#: src/window.py:244
msgid "Dropped item is not a valid image"
msgstr "Upuszczony element nie jest prawidłowym obrazem"

#: src/file_chooser.py:38 src/file_chooser.py:101
msgid "Select a file"
msgstr "Wybierz plik"

#: src/file_chooser.py:45
msgid "Supported image files"
msgstr "Obsługiwane pliki obrazów"

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:79
#, python-brace-format
msgid "Unable to save “{display_name}”"
msgstr "Nie można zapisać „{display_name}”"

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:88
#, python-brace-format
msgid "“{display_name}” saved"
msgstr "„{display_name}” zapisane"

#: src/file_chooser.py:90
msgid "Open"
msgstr "Otwarty"

#: src/pasting.py:48
msgid "No image found in clipboard"
msgstr "Nie znaleziono obrazu w schowku"

#: src/gtk/window.blp:17 src/gtk/tips-dialog.blp:5
msgid "Tips"
msgstr "Wskazówki"

#: src/gtk/window.blp:26
msgid "Main Menu"
msgstr "Menu główne"

#: src/gtk/window.blp:90
msgid "Create ASCII Art"
msgstr "Tworzenie grafiki ASCII"

#: src/gtk/window.blp:91
msgid "Open or drag and drop an image to generate an ASCII art version of it"
msgstr "Otwórz lub przeciągnij i upuść obraz, aby wygenerować jego wersję ASCII"

#: src/gtk/window.blp:96
msgid "Open File…"
msgstr "Otwórz plik…"

#: src/gtk/window.blp:158
msgid "Width (in characters)"
msgstr "Szerokość (w znakach)"

#: src/gtk/window.blp:184
msgid "Copy to clipboard"
msgstr "Kopiuj do schowka"

#: src/gtk/window.blp:192
msgid "Save to file"
msgstr "Zapisz do pliku"

#: src/gtk/window.blp:211
msgid "_Open File…"
msgstr "_Otwórz plik…"

#: src/gtk/window.blp:218
msgid "_Keyboard Shortcuts"
msgstr "_Skróty klawiaturowe"

#: src/gtk/window.blp:223
msgid "_About Letterpress"
msgstr "_O Letterpress"

#: src/gtk/zoom-box.blp:12
msgid "Zoom out"
msgstr "Pomniejszanie"

#: src/gtk/zoom-box.blp:29
msgid "Reset zoom"
msgstr "Resetuj zoom"

#: src/gtk/zoom-box.blp:42
msgid "Zoom in"
msgstr "Powiększanie"

#: src/gtk/tips-dialog.blp:22
msgid "Pictures used for ASCII art should have the following qualities:"
msgstr "Obrazy używane w grafice ASCII powinny mieć następujące cechy:"

#: src/gtk/tips-dialog.blp:49
msgid "High Contrast"
msgstr "Wysoki kontrast"

#: src/gtk/tips-dialog.blp:66
msgid "Vibrant Colors"
msgstr "Żywe kolory"

#: src/gtk/tips-dialog.blp:83
msgid "Defined Edges"
msgstr "Zdefiniowane krawędzie"

#: src/gtk/tips-dialog.blp:100
msgid "Recognizable Shapes"
msgstr "Rozpoznawalne kształty"

#: src/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: src/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr "Otwórz plik"

#: src/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Paste Image"
msgstr "Wklej obraz"

#: src/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Pokaż skróty"

#: src/gtk/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Quit"
msgstr "Odejdź"

#: src/gtk/help-overlay.blp:35
msgctxt "shortcut window"
msgid "Text View"
msgstr "Widok tekstowy"

#: src/gtk/help-overlay.blp:38
msgctxt "shortcut window"
msgid "Increase Output Width"
msgstr "Zwiększenie szerokości wyjścia"

#: src/gtk/help-overlay.blp:43
msgctxt "shortcut window"
msgid "Decrease Output Width"
msgstr "Zmniejszenie szerokości wyjścia"

#: src/gtk/help-overlay.blp:48
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Powiększanie"

#: src/gtk/help-overlay.blp:53
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Powiększenie"

#: src/gtk/help-overlay.blp:58
msgctxt "shortcut window"
msgid "Reset zoom"
msgstr "Resetuj zoom"

#: src/gtk/help-overlay.blp:63
msgctxt "shortcut window"
msgid "Copy Output to Clipboard"
msgstr "Kopiowanie wyjścia do schowka"

#: src/gtk/help-overlay.blp:68
msgctxt "shortcut window"
msgid "Save Output to File"
msgstr "Zapisz wyjście do pliku"
